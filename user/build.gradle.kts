val mainClass: String = "io.forgo.user.Application"

application {
  mainClassName = mainClass
}

jib {
  val publish: Publish by project.extra

  extraDirectories.setPaths(File("${buildDir}/jib-dir"))
  from {
    // base image
    image = "adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim"
  }
  to {
    image = repository(publish)
    auth {
      username = publish.username
      password = publish.password
    }
  }
  container {
    creationTime = publish.created
    labels = ociAnnotations(publish)
    ports = listOf("8080")
    jvmFlags = listOf("-Dcom.sun.management.jmxremote", "-noverify")
    mainClass = mainClass
  }
}