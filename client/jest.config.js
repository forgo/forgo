module.exports = {
  verbose: true,
  coverageDirectory: './build/coverage/',
  collectCoverage: true,
  collectCoverageFrom: [ 'pages/**/*.{js,jsx}' ],
  coverageReporters: [ 'text-summary', 'html', 'lcov' ],
  coveragePathIgnorePatterns: [
    './.next/',
    './node_modules/',
    './__test__/',
    './__mocks__/',
    './nginx/',
  ],
  reporters: [
    'default',
    [ 'jest-junit', {outputDirectory: './build/junit'} ],
  ],

  preset: 'ts-jest/presets/js-with-ts',
  testEnvironment: 'node',
  moduleFileExtensions: [ 'ts', 'tsx', 'js', 'jsx', 'json' ],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js',
    '\\.(css|less)$': '<rootDir>/__mocks__/styleMock.js',
  },
  transform: {
    '^.+\\.(ts|tsx|js|jsx)$': 'ts-jest',
  },
  testMatch: [ '**/__tests__/*.(ts|tsx|js|jsx)' ],
  setupFiles: [ './jest.setup.js' ],
  testPathIgnorePatterns: [ './.next/', './node_modules/' ],
  globals: {
    'ts-jest': {
      tsConfig: 'tsconfig.json',
    },
  },
}
