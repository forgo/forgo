import React from 'react'

const styleContainer = {
  // display: 'flex',
  // justifyContent: 'space-between',
  // alignItems: 'center',
}

const styleTable = {
  borderCollapse: 'collapse',
}

const styleCell = {
  border: '1px solid white',
  padding: '0.309em',
}

const authorInfo = (author, projectName) => {
  const info = author.match(/^(.*?)<(.*?)>\s.*\((.*?)\)$/)
  const name = info[1].trim()
  const email = info[2].trim()
  const website = info[3].trim()

  const n = 'forgo-client'
  const subject = encodeURIComponent(`Message regarding '${projectName}'`)
  return (
    <span>
      {name}&nbsp; &lt;<a href={`mailto:${email}?subject=${subject}`}>
        email
      </a>&gt;&nbsp; (<a href={website}>website</a>)
    </span>
  )
}

const link = (href, text = undefined) => {
  return <a href={href}>{text ? text : href}</a>
}

const projectLink = (name, repository) => {
  // if the repo url has a `.git` suffix (e.g. - GitLab), remove it
  const url = repository.url.replace(/\.git$/, '')
  return link(`${url}/tree/master/${repository.directory}`, name)
}

const licenseLink = license => {
  return link(`https://spdx.org/licenses/${license}`, license)
}

export const About = () => {
  const projectInfo = {
    name: projectLink(PACKAGE_JSON.name, PACKAGE_JSON.repository),
    version: PACKAGE_JSON.version,
    author: authorInfo(PACKAGE_JSON.author, PACKAGE_JSON.name),
    source: link(PACKAGE_JSON.homepage),
    issues: link(PACKAGE_JSON.bugs.url),
    license: licenseLink(PACKAGE_JSON.license),
  }

  const tableRows = Object.entries(projectInfo).map(([ key, value ]) => {
    return (
      <tr key={key}>
        <th style={styleCell}>{key}</th>
        <td style={styleCell}>{value}</td>
      </tr>
    )
  })

  return (
    <ul style={styleContainer}>
      <h1>Forgo</h1>
      <p>{PACKAGE_JSON.description}</p>
      <table style={styleTable}>
        <tbody>{tableRows}</tbody>
      </table>
    </ul>
  )
}
