import React from 'react'

const styleContainer = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
}

export const Footer = () => {
  return <div style={styleContainer} />
}
