// import React, {useEffect, useRef, useState} from 'react'
//
// export function mapFromObject(obj){
//   function* entries(obj){
//     for (let key in obj) yield [ key, obj[key] ]
//   }
//   return new Map(entries(obj))
// }
//
// const styleList = {
//   display: 'flex',
//   flexDirection: 'column',
//   flexWrap: 'nowrap',
// }
//
// const styleGrid = {
//   display: 'flex',
//   flexDirection: 'row',
//   flexWrap: 'wrap',
//   justifyContent: 'center',
//   alignItems: 'flex-start',
//   alignContent: 'flex-start',
// }
//
// function useItem() {
//   const itemRef = useRef()
//   useEffect(
//     () => {
//       itemRef.contains(document.activeElement)
//     },
//
//   )
// }
//
// }
//
// function usePrevious(value, defaultValue = undefined){
//   const ref = useRef()
//   useEffect(
//     () => {
//       ref.current = value
//     },
//     [ value ]
//   )
//   return ref.current ? ref.current : defaultValue
// }
//
// function useItems(items){
//   const [ itemsMap, setItemsMap ] = useState(mapFromObject(items))
//
//   // keep track of previous items map
//   const previousItemsMap = usePrevious(itemsMap, new Map())
//
//   const [ focusedKey, setFocusedKey ] = useState(undefined)
//   const [ lastItem, setLastItem ] = useState(undefined)
//
//   // this effect tracks when the items supplied to ListView changes
//   useEffect(
//     () => {
//       // ensure our items are stored as a JavaScript `Map`
//       // as a `Map` can iterate elements in insertion order and easily
//       // retrieve the size of the items, without counting keys
//       setItemsMap(mapFromObject(items))
//     },
//     [ items ]
//   )
//
//   useEffect(
//     () => {
//     },
//     [action]
//   )
//
//   // this effect tracks when the derived itemsMap changes
//   useEffect(
//     () => {
//       const keysAfter = [ ...itemsMap.keys() ]
//
//       const nextKey = keysAfter.indexOf(lastItem) + 1
//       setFocusedKey(keysAfter[nextKey])
//
//       const lastKey =
//         itemsMap.size > 0
//           ? Array.from(itemsMap)[itemsMap.size - 1][0]
//           : undefined
//       setLastItem(lastKey)
//     },
//     [ itemsMap ]
//   )
//
//   return [ itemsMap, focusedKey, setFocusedKey ]
// }
//
// export default function ListView(props){
//   const {
//     items,
//     ListItemComponent,
//     GridItemComponent,
//     propsForItem,
//   } = props
//
//   const [ itemsMap, focusedKey, setFocusedKey ] = useItems(props.items, props.action)
//
//   const showAsGrid = !!props.showAsGrid && !!props.GridItemComponent
//
//
//   let itemElements = []
//   itemsMap.forEach((item, key) => {
//     let itemElement = null
//
//     if (!showAsGrid && ListItemComponent) {
//       // list item element
//       itemElement = (
//         <ListItemComponent
//           key={key}
//           itemId={key}
//           item={item}
//         />
//       )
//     }
//     else if (showAsGrid && GridItemComponent) {
//       // grid item element
//       itemElement = (
//         <GridItemComponent
//           key={key}
//           itemId={key}
//           item={item}
//         />
//       )
//     }
//     else {
//       // default item element
//       itemElement = (
//         <div
//           key={key}
//           children={key}
//         />
//       )
//     }
//     itemElements.push(itemElement)
//   })
//
//   return <div style={showAsGrid ? styleGrid : styleList}>{itemElements} />
// }
