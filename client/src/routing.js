export const ROUTE = Object.freeze({
  home: {path: '/'},
  login: {path: '/login'},
  about: {path: '/about'},
  error: {path: '/error'},
})
