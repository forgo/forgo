import {useMutation} from '@apollo/react-hooks'
import gql from 'graphql-tag'
import {ALL_POSTS_QUERY, allPostsQueryVars} from './PostList'
import TextButton from './TextButton'

const styleSubmitForm = {
  borderBottom: '1px solid red',
}

const styleSubmitFormTitle = {
  fontSize: '3.09em',
}

const styleSubmitFormInput = {
  display: 'block',
  marginTop: '1.05em',
}

const CREATE_POST_MUTATION = gql`
  mutation createPost($title: String!, $url: String!) {
    createPost(title: $title, url: $url) {
      id
      title
      votes
      url
      createdAt
    }
  }
`

export default function Submit(){
  const [ createPost, {loading} ] = useMutation(CREATE_POST_MUTATION)

  const handleSubmit = event => {
    event.preventDefault()
    const form = event.target
    const formData = new window.FormData(form)
    const title = formData.get('title')
    const url = formData.get('url')
    form.reset()

    createPost({
      variables: {title, url},
      update: (proxy, {data: {createPost}}) => {
        const data = proxy.readQuery({
          query: ALL_POSTS_QUERY,
          variables: allPostsQueryVars,
        })
        // Update the cache with the new post at the top of the
        proxy.writeQuery({
          query: ALL_POSTS_QUERY,
          data: {
            ...data,
            allPosts: [ createPost, ...data.allPosts ],
          },
          variables: allPostsQueryVars,
        })
      },
    })
  }

  return (
    <form style={styleSubmitForm} onSubmit={handleSubmit}>
      <h1 style={styleSubmitFormTitle}>Submit</h1>
      <input
        style={styleSubmitFormInput}
        placeholder="title"
        name="title"
        type="text"
        required
      />
      <input
        style={styleSubmitFormInput}
        placeholder="url"
        name="url"
        type="url"
        required
      />
      <TextButton text={'Submit'} buttonType={'submit'} disabled={loading} />
    </form>
  )
}
