import React from 'react'
import Head from 'next/head'

const head = () => {
  return (
    <Head>
      <title>forgo.io</title>
    </Head>
  )
}

export default head
