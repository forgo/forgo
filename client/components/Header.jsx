import Link from 'next/link'
import {withRouter} from 'next/router'

const styleLink = isActive => {
  return {
    fontSize: '1.618em',
    margin: '0 0.309em',
    textDecoration: isActive ? 'underline' : 'none',
  }
}

const Path = {
  HOME: '/',
  CLIENT_ONLY: '/client-only',
  ABOUT: '/about',
}

const isActiveRoute = (path, pathname) => {
  return path === pathname
}

const HeaderLink = ({text, path, pathname}) => (
  <Link href={path}>
    <a style={styleLink(isActiveRoute(path, pathname))}>{text}</a>
  </Link>
)

const Header = ({router: {pathname}}) => (
  <header>
    <HeaderLink text={'Home'} path={Path.HOME} pathname={pathname} />
    <HeaderLink
      text={'Client-Only'}
      path={Path.CLIENT_ONLY}
      pathname={pathname}
    />
    <HeaderLink text={'About'} path={Path.ABOUT} pathname={pathname} />
  </header>
)

export default withRouter(Header)
