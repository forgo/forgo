import {useQuery} from '@apollo/react-hooks'
import {NetworkStatus} from 'apollo-client'
import gql from 'graphql-tag'
import ErrorMessage from './ErrorMessage'
import PostUpvoter from './PostUpvoter'
import TextButton from './TextButton'
import TextButtonPreview from './TextButtonPreview'

const styleSection = {
  paddingBottom: '3.09em',
}

const stylePostList = {
  margin: 0,
  padding: 0,
}

const stylePostListItem = {
  display: 'block',
  marginBottom: '1.05em',
}

const stylePost = {
  display: 'flex',
  alignItems: 'center',
}

const stylePostNumber = {
  fontSize: '1.618em',
  marginRight: '1em',
}

const stylePostLink = {
  fontSize: '1.618em',
  marginRight: '3.09em',
  textDecoration: 'none',
  paddingBottom: 0,
  border: 0,
}

export const ALL_POSTS_QUERY = gql`
  query allPosts($first: Int!, $skip: Int!) {
    allPosts(orderBy: createdAt_DESC, first: $first, skip: $skip) {
      id
      title
      votes
      url
      createdAt
    }
    _allPostsMeta {
      count
    }
  }
`
export const allPostsQueryVars = {
  skip: 0,
  first: 10,
}

export default function PostList(){
  const {
    loading,
    error,
    data,
    fetchMore,
    networkStatus,
  } = useQuery(ALL_POSTS_QUERY, {
    variables: allPostsQueryVars,
    // Setting this value to true will make the component rerender when
    // the "networkStatus" changes, so we are able to know if it is fetching
    // more data
    notifyOnNetworkStatusChange: true,
  })

  const loadingMorePosts = networkStatus === NetworkStatus.fetchMore

  const loadMorePosts = () => {
    fetchMore({
      variables: {
        skip: allPosts.length,
      },
      updateQuery: (previousResult, {fetchMoreResult}) => {
        if (!fetchMoreResult) {
          return previousResult
        }
        return Object.assign({}, previousResult, {
          // Append the new posts results to the old one
          allPosts: [ ...previousResult.allPosts, ...fetchMoreResult.allPosts ],
        })
      },
    })
  }

  if (error) return <ErrorMessage message="Error loading posts." />
  if (loading && !loadingMorePosts) return <div>Loading</div>

  const {allPosts, _allPostsMeta} = data
  const areMorePosts = allPosts.length < _allPostsMeta.count

  return (
    <section style={styleSection}>
      <ul style={stylePostList}>
        {allPosts.map((post, index) => (
          <li key={post.id} style={stylePostListItem}>
            <div style={stylePost}>
              <span style={stylePostNumber}>{index + 1}. </span>
              <a style={stylePostLink} href={post.url}>
                {post.title}
              </a>
              <PostUpvoter id={post.id} votes={post.votes} />
            </div>
          </li>
        ))}
      </ul>
      {areMorePosts && (
        <TextButton
          text={loadingMorePosts ? 'Loading...' : 'Show More'}
          onClick={() => loadMorePosts()}
          disabled={loadingMorePosts}
        />
      )}
      <TextButtonPreview />
    </section>
  )
}
