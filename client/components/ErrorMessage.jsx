const styleErrorMessage = {
  padding: '0.618em',
  fontSize: '1.618em',
  color: 'white',
  backgroundColor: 'red',
}

export default ({message}) => <aside style={styleErrorMessage}>{message}</aside>
