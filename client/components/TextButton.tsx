import * as React from 'react'
import {useRef, useState, useEffect, Fragment, Component} from 'react'

type TextButtonMouseEvent = React.MouseEvent<
  HTMLButtonElement | HTMLAnchorElement,
  MouseEvent
>
type TextButtonKeyEvent = React.KeyboardEvent<
  HTMLButtonElement | HTMLAnchorElement
>
type TextButtonFocusEvent = React.FocusEvent<
  HTMLButtonElement | HTMLAnchorElement
>

type TextButtonProps = {
  display?: string
  width?: string
  text: string
  icon?: Component
  alignIcon?: 'left' | 'right'
  textColorHex?: string
  textColorActiveHex?: string
  backgroundColorHex?: string
  backgroundColorActiveHex?: string
  shadowColorHex?: string
  onClick?: () => void
  disabled?: boolean
  disabledText?: string
  name?: string
  buttonType?: 'submit' | 'button' | 'reset'
  value?: string
  // form specific
  form?: string
  // type="submit"
  formAction?: string
  formEncType?:
    | 'application/x-www-form-urlencoded'
    | 'multipart/form-data'
    | 'text/plain'
  formMethod?: 'get' | 'post'
  formNoValidate?: boolean
  formTarget?: '_blank' | '_self' | '_parent' | '_top' | string
}

function hexToRGB(hex: string){
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    let r = parseInt(hex.slice(1, 3), 16),
      g = parseInt(hex.slice(3, 5), 16),
      b = parseInt(hex.slice(5, 7), 16)
    return `${r}, ${g}, ${b}`
  }
  throw new Error('Bad Hex')
}

const DEFAULT_BUTTON_COLOR = '255, 255, 255'
const DEFAULT_BUTTON_ACTIVE_COLOR = '255, 255, 255'
const DEFAULT_BUTTON_BACKGROUND_COLOR = '45, 123, 45'
const DEFAULT_BUTTON_BACKGROUND_ACTIVE_COLOR = '0, 72, 0'
const DEFAULT_BUTTON_SHADOW_COLOR = '0, 0, 0'

const BUTTON_COLOR = (textColorHex: string, opacity: number) => {
  return `rgba(${textColorHex
    ? hexToRGB(textColorHex)
    : DEFAULT_BUTTON_COLOR}, ${opacity})`
}

const BUTTON_ACTIVE_COLOR = (textColorActiveHex: string, opacity: number) => {
  return `rgba(${textColorActiveHex
    ? hexToRGB(textColorActiveHex)
    : DEFAULT_BUTTON_ACTIVE_COLOR}, ${opacity})`
}

const BUTTON_BACKGROUND_COLOR = (
  backgroundColorHex: string,
  opacity: number
) => {
  return `rgba(${backgroundColorHex
    ? hexToRGB(backgroundColorHex)
    : DEFAULT_BUTTON_BACKGROUND_COLOR}, ${opacity})`
}

const BUTTON_BACKGROUND_ACTIVE_COLOR = (
  backgroundColorActiveHex: string,
  opacity: number
) => {
  return `rgba(${backgroundColorActiveHex
    ? hexToRGB(backgroundColorActiveHex)
    : DEFAULT_BUTTON_BACKGROUND_ACTIVE_COLOR}, ${opacity})`
}

const BUTTON_SHADOW_COLOR = (shadowColorHex: string, on: boolean) => {
  return `${on ? '0 0.309em 0.618em' : '0 0 0'} rgba(${shadowColorHex
    ? hexToRGB(shadowColorHex)
    : DEFAULT_BUTTON_SHADOW_COLOR}, 0.309)`
}

const Key = {
  ENTER: 13,
  SPACE: 32,
}

const isTriggerKey = (keyCode: number) => {
  let is = false
  switch (keyCode) {
    case Key.ENTER:
      is = true
      break
    case Key.SPACE:
      is = true
      break
  }
  return is
}

const styleWrapper = (
  display: string,
  width: string,
  focusing: boolean,
  pressing: boolean,
  backgroundColorHex: string,
  backgroundColorActiveHex: string
) => {
  return {
    display: display ? display : 'inline-block',
    width: width ? width : 'fit-content',
    padding: '0.305em',
    borderRadius: '0.618em',
    boxSizing: 'border-box',
    border: `2px dashed ${BUTTON_BACKGROUND_COLOR(
      pressing ? backgroundColorActiveHex : backgroundColorHex,
      focusing ? 1 : 0
    )}`,
  }
}

const styleDefault = (
  textColorHex: string,
  backgroundColorHex: string,
  shadowColorHex: string
) => {
  return {
    display: 'flex',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    textDecoration: 'none',
    fontSize: '1em',
    fontFamily:
      'Lucida Grande,Lucida Sans Unicode,Lucida Sans,Geneva,Verdana,sans-serif',
    margin: 0,
    padding: 'calc(0.309em - 1px) 0.618em calc(0.309em + 1px) 0.618em',
    color: BUTTON_COLOR(textColorHex, 1),
    background: BUTTON_BACKGROUND_COLOR(backgroundColorHex, 1),
    transition: 'all 200ms ease 0s',
    borderRadius: '0.618em',
    border: '2px solid white',
    boxShadow: BUTTON_SHADOW_COLOR(shadowColorHex, true),
    userSelect: 'none',
  }
}

const styleHoverDefault = {
  cursor: 'pointer',
}
const stylePressDefault = (
  textColorActiveHex: string,
  backgroundColorActiveHex: string,
  shadowColorHex: string
) => {
  return {
    color: BUTTON_ACTIVE_COLOR(textColorActiveHex, 1),
    background: BUTTON_BACKGROUND_ACTIVE_COLOR(backgroundColorActiveHex, 1),
    boxShadow: BUTTON_SHADOW_COLOR(shadowColorHex, false),
  }
}
const styleFocusDefault = {
  outline: 'none',
}
const styleDisableDefault = (
  backgroundColorHex: string,
  shadowColorHex: string
) => {
  return {
    opacity: 0.618,
    cursor: 'not-allowed',
    background: BUTTON_BACKGROUND_COLOR(backgroundColorHex, 1),
    boxShadow: BUTTON_SHADOW_COLOR(shadowColorHex, true),
  }
}

const styleText = (alignIcon: string) => {
  return {
    position: 'relative',
    display: 'flex',
    flexDirection: !alignIcon || alignIcon === 'left' ? 'row' : 'row-reverse',
  }
}

const styleTextUnderline = (
  pressing: boolean,
  disabled: boolean,
  textColorHex: string
) => {
  return {
    position: 'absolute',
    width: '100%',
    height: '1px',
    bottom: '-1px',
    left: 0,
    visibility: 'hidden',
    transform: 'scaleX(0)',
    transition: 'all 200ms ease-in-out 0s',
    background: `linear-gradient(
      to right, ${BUTTON_COLOR(textColorHex, 1)} 0%, 
      ${BUTTON_COLOR(textColorHex, 0)} 50%, 
      ${BUTTON_COLOR(textColorHex, 1)} 100%
    )`,
    ...pressing && !disabled
      ? {visibility: 'visible', transform: 'scaleX(1)'}
      : {},
  }
}

const styleIconWrapper = {
  display: 'flex',
  flexDirection: 'column-reverse',
}

function usePrevious(value: boolean){
  const ref = useRef<boolean | null>(false)
  useEffect(
    () => {
      ref.current = value
    },
    [ value ]
  )
  return ref.current
}

const TextButton = ({
  display,
  width,
  text,
  icon,
  alignIcon,
  textColorHex,
  textColorActiveHex,
  backgroundColorHex,
  backgroundColorActiveHex,
  shadowColorHex,
  onClick,
  disabled,
  disabledText,
  name,
  buttonType,
  value,
  form,
  formAction,
  formEncType,
  formMethod,
  formNoValidate,
  formTarget,
}: TextButtonProps) => {
  const buttonRef = useRef(null)

  const [ hovering, setHovering ] = useState(false)
  const [ pressing, setPressing ] = useState(false)
  const [ pressingGlobal, setPressingGlobal ] = useState(false)
  const [ focusing, setFocusing ] = useState(false)

  const previousPressingGlobal: boolean = usePrevious(pressingGlobal)

  const handleClick = (event: TextButtonMouseEvent) => {
    if (onClick) {
      onClick()
    }
  }

  const handleMouseOver = (event: TextButtonMouseEvent) => {
    setHovering(true)
    setPressing(previousPressingGlobal)
  }

  const handleMouseOut = (event: TextButtonMouseEvent) => {
    setHovering(false)
    setPressing(false)
  }

  const handleGlobalMouseUp = (event: MouseEvent) => {
    setPressingGlobal(false)
  }

  const handleGlobalMouseDown = (event: MouseEvent) => {
    setPressingGlobal(true)
  }

  const handleMouseDown = (event: TextButtonMouseEvent) => {
    setPressing(true)
  }

  const handleMouseUp = (event: TextButtonMouseEvent) => {
    setPressing(false)
  }

  const handleKeyDown = (event: TextButtonKeyEvent) => {
    if (isTriggerKey(event.keyCode)) {
      setPressing(true)
      event.preventDefault()
    }
  }

  const handleKeyUp = (event: TextButtonKeyEvent) => {
    if (isTriggerKey(event.keyCode)) {
      setPressing(false)
      if (onClick) {
        onClick()
      }
      event.preventDefault()
    }
  }

  const handleFocus = (event: TextButtonFocusEvent) => {
    setFocusing(true)
  }

  const handleBlur = (event: TextButtonFocusEvent) => {
    console.log('mmk')
    setFocusing(false)
  }

  useEffect(() => {
    document.addEventListener('mouseup', handleGlobalMouseUp, false)
    document.addEventListener('mousedown', handleGlobalMouseDown, false)

    return () => {
      document.removeEventListener('mouseup', handleGlobalMouseUp, false)
      document.removeEventListener('mousedown', handleGlobalMouseDown, false)
    }
  }, [])

  useEffect(
    () => {
      // ensure focus styles are removed when button is disabled
      if (focusing && disabled) {
        setFocusing(false)
      }
    },
    [ focusing, disabled ]
  )

  const styleButtonMerged = {
    ...styleDefault(textColorHex, backgroundColorHex, shadowColorHex),
    ...hovering ? styleHoverDefault : {},
    ...pressing
      ? stylePressDefault(
          textColorActiveHex,
          backgroundColorActiveHex,
          shadowColorHex
        )
      : {},
    ...focusing ? styleFocusDefault : {},
    ...disabled ? styleDisableDefault(backgroundColorHex, shadowColorHex) : {},
  }

  let formProps =
    buttonType === 'submit'
      ? {
          form,
          formAction,
          formEncType,
          formMethod,
          formNoValidate,
          formTarget,
        }
      : {}

  const iconElement = icon ? (
    <div style={styleIconWrapper as any}>{icon}</div>
  ) : null

  return (
    <Fragment>
      <div
        style={
          styleWrapper(
            display,
            width,
            focusing,
            pressing,
            backgroundColorHex,
            backgroundColorActiveHex
          ) as any
        }
      >
        <button
          ref={buttonRef}
          title={text}
          aria-label={text}
          name={name}
          value={value}
          type={buttonType}
          style={styleButtonMerged as any}
          onClick={handleClick}
          onMouseOver={handleMouseOver}
          onMouseOut={handleMouseOut}
          onMouseDown={handleMouseDown}
          onMouseUp={handleMouseUp}
          onKeyDown={handleKeyDown}
          onKeyUp={handleKeyUp}
          onFocus={handleFocus}
          onBlur={handleBlur}
          disabled={disabled}
          aria-disabled={disabled}
          {...formProps}
        >
          <span style={styleText(alignIcon) as any}>
            {iconElement}
            {disabled ? disabledText ? disabledText : text : text}
            <span
              style={
                styleTextUnderline(pressing, disabled, textColorHex) as any
              }
            />
          </span>
        </button>
      </div>
    </Fragment>
  )
}

export default TextButton
