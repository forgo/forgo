const styleInfoBox = {
  margin: '3.09em 0',
  padding: '3.09em 0',
}

export default ({children}) => <div style={styleInfoBox}>{children}</div>
