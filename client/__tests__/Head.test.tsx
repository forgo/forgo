import {shallow} from 'enzyme'
import * as React from 'react'

import Head from '../components/Head'

describe('Header has proper content', () => {
  test('on index page', () => {
    const head = shallow(<Head />)
    expect(head.find('title').text()).toEqual('forgo.io')
  })
})
