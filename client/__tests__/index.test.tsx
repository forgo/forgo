import {shallow} from 'enzyme'
import * as React from 'react'

import {IndexPageComponent} from '../pages/index'
import App from '../components/App'

describe('IndexPage', () => {
  test('defines one head section', () => {
    const app = shallow(<IndexPageComponent />)
    expect(app.find(App)).toHaveLength(1)
  })
})
