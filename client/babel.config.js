module.exports = function(api){
  const presetsDevelopment = [ 'next/babel' ]

  const presetsTest = [ 'next/babel' ]

  const presetsProduction = [ 'next/babel' ]

  let presets = []
  switch (api.env()) {
    case 'development':
      presets = presetsDevelopment
      break
    case 'test':
      presets = presetsTest
      break
    case 'production':
      presets = presetsProduction
      break
  }

  const plugins = [ [ 'graphql-tag' ] ]

  return {
    presets: presets,
    plugins: plugins,
  }
}
