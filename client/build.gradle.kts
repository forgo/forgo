jib {
  val publish: Publish by project.extra
  val jibExtraDir: String by project.extra

  extraDirectories.setPaths(File(jibExtraDir))
  extraDirectories.permissions = mapOf(
      // make entrypoint script executable
      Pair("/srv/www/${publish.title}/next.sh", "755")
  )
  from {
    // base image
    image = "node:alpine"
  }
  to {
    image = repository(publish)
    auth {
      username = publish.username
      password = publish.password
    }
  }
  container {
    creationTime = publish.created
    labels = ociAnnotations(publish)
    ports = listOf("3000")
    workingDirectory = "/srv/www/${publish.title}"
    entrypoint = listOf("./next.sh")
  }
}