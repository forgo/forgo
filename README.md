# forgo

Experimental project to dive into the latest practices in software engineering.

### Infrastructure & Maintenance Goals
- Leverage Gradle to the fullest
  - latest Kotlin plugin (null safety, type inference, etc.)
  - prevent duplicated logic and other common technical debts
    - version mismatch between subprojects
    - sharing common plugin configurations between subprojects
    - sync dynamic `package.json` values for Node subprojects
  - conform to latest standards
    - [Open Container Initiative](https://github.com/opencontainers/image-spec/blob/master/spec.md)
      - [Jib](https://github.com/GoogleContainerTools/jib/tree/master/jib-gradle-plugin) `format = OCI`
      - [Annotations](https://github.com/opencontainers/image-spec/blob/master/annotations.md) derived consistently from build and version control
  - abstract common but burdensome tasks
    - publishing containers to a registry should be simple and consistent
      - define `data class Publish` values once and dynamically for all projects leveraging `Jib`
      - switch easily between registry mechanisms (e.g. - DockerHub, GitLab)
      
### Feature Goals
- Use latest and greatest tools designed for performance and scalability
  - API:
    - [Micronaut](https://micronaut.io/)
      - fast startup, low memory consumption ([GraalVM](https://www.graalvm.org/))
      - declarative, reactive, compile-time http clients
      - easily testable
      - ready for serverless
  - UI: 
    - [Next.js](https://nextjs.org/)
      - abstracts away much of the boilerplate webpack configuration
      - abstracts away the hard parts of [Server-Side Rendering](https://nextjs.org/features/server-side-rendering)
    
    
   

  
      
     
    


## Quick Start Guide

This guide covers how you can quickly get started developing with the following features.

### System Requirements

|          | Version | Description |
|----------|---------|-------------|
| Java     | [JDK8, JDK11, JDK13](https://www.oracle.com/technetwork/java/javase/downloads/index.html) | ≥ for Gradle 6 and everyting JVM-related |
| Docker   | [Mac](https://docs.docker.com/docker-for-mac/install/), [Windows](https://docs.docker.com/docker-for-windows/install/), [Linux](https://docs.docker.com/install/linux/ubuntu/) | for local containerization and Kubernetes development |
| Helm     | [2.16.1](https://github.com/helm/helm/releases/tag/v2.16.1) | manage Kubernetes definitions for whole cluster |
| Skaffold | [1.1.0](https://github.com/GoogleContainerTools/skaffold/releases/tag/v1.1.0) | simplify continuous development on local cluster | 
| Node     | [10.16.3](https://github.com/nodejs/node/releases/tag/v10.16.3) | _see NPM_ |
| NPM      | [6.9.0](https://github.com/npm/cli/releases/tag/v6.9.0) | hot-reload dev client via without Kubernetes |
