import com.google.gson.Gson
import com.google.gson.JsonObject
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import java.io.File
import java.io.FileWriter

data class PackageJSON(
    val name: String,
    val version: String,
    val description: String,
    val author: String,
    val license: String,
    val homepage: String,
    val repositoryUrl: String,
    val repositoryDirectory: String,
    val bugsUrl: String
)

open class PackageJSONTask : DefaultTask() {

  var updates: PackageJSON? = null

  // utilities to help keep values in package.json files up-to-date with gradle values
  private fun updatePackageJSON(filePath: String, packageJSON: PackageJSON?): Boolean {

    // nothing given to update
    if(packageJSON == null) {
      return false
    }

    // create Gson instance for deserializing/serializing
    val gson = Gson()

    try {
      // read the JSON file as a string
      val jsonString: String = File(filePath).readText(Charsets.UTF_8)

      // deserialize JSON file into JsonObject
      val jsonObject = gson.fromJson(jsonString, JsonObject::class.java)

      // update root-level keys
      jsonObject.addProperty("name", packageJSON.name)
      jsonObject.addProperty("version", packageJSON.version)
      jsonObject.addProperty("description", packageJSON.description)
      jsonObject.addProperty("author", packageJSON.author)
      jsonObject.addProperty("license", packageJSON.license)
      jsonObject.addProperty("homepage", packageJSON.homepage)

      // update keys under 'repository' section
      jsonObject.getAsJsonObject("repository").addProperty("url", packageJSON.repositoryUrl)
      jsonObject.getAsJsonObject("repository").addProperty("directory", packageJSON.repositoryDirectory)

      // update keys under 'bugs' section
      jsonObject.getAsJsonObject("bugs").addProperty("url", packageJSON.bugsUrl)

      // re-serialize updated JSON and write back to original file with pretty formatting
      FileWriter(filePath, Charsets.UTF_8).use { writer ->
        gson.newBuilder().setPrettyPrinting().create().toJson(jsonObject, writer)
      }
    }
    catch(e: Exception) {
      // if anything goes wrong in the process, return false
      return false
    }
    return true
  }

  @TaskAction
  fun update() {
    logger.lifecycle("Running 'updatePackageJSON' task on ${project.name}")
    val packageJsonPath = project.projectDir.absolutePath + "/package.json"
    if(!updatePackageJSON(packageJsonPath, updates)) {
      logger.warn("'WARNING: package.json' was NOT updated in '${project.name}'")
    }
  }
}