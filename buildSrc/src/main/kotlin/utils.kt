import java.io.File

// constants
object License {
  const val MIT: String = "MIT"
}

object Registries {
  const val DOCKER_HUB = "registry.hub.docker.com"
  const val GITLAB = "registry.gitlab.com"
}

object Versions {
  // this must be a valid semantic version for some downstream commands
  // like `npm version ${version}` that expect such a format, but having
  // a default allows us to spot when a published docker image was created locally, for example
  const val DEFAULT: String = "0.0.0"

  // https://www.opencontainers.org/
  // https://github.com/opencontainers/image-spec/blob/master/annotations.md#annotations
  const val LABEL_SCHEMA: String = "1.0"

  const val MICRONAUT: String = "1.2.8"

  const val NODE: String = "10.16.3"
  const val NPM: String = "6.9.0"
}

// data classes
data class Author(
    val name: String,
    val email: String,
    val website: String
)

data class Publish(
    val created: String,
    val title: String,
    val project: String,
    val description: String,
    val documentation: String,
    val authors: String,
    val url: String,
    val source: String,
    val revision: String,
    val vendor: String,
    val version: String,
    val licenses: String,
    val registryUrl: String,
    val username: String,
    val password: String
)

//data class Node(
//    val version: String,
//    val npmVersion: String,
//    val workDir: File,
//    val npmWorkDir: File,
//    val nodeModulesDir: File
//)

// utility functions
fun environment(variable: String, default: String = ""): String {
  return (System.getenv(variable) ?: default).trim()
}

fun String.runShell(dir: File? = null, quiet: Boolean = true): String? {
  val builder: ProcessBuilder = ProcessBuilder("/bin/sh", "-c", this)
      .redirectErrorStream(true)
      .directory(dir)
  val process: Process = builder.start()
  val exitCode: Int = process.waitFor()
  var output: String? = process.inputStream.readBytes().toString(Charsets.UTF_8).trim()
  if(exitCode != 0) {
    // print stderr for visibility, since we return `null` on error
    println("\nError running: `${this}`\n")
    println("${output}\n")
    output = null
  }
  if(!quiet && exitCode == 0) {
    // print stdout when `quiet = false` to prevent excessive output by default
    println("\nRunning: `${this}`\n")
    println("${output}\n")
  }
  return output
}

fun isLocal(version: String, localPrefix: String): Boolean {
  return version.startsWith(localPrefix)
}

fun isCI(): Boolean {
  return (System.getenv("CI") ?: "").toBoolean()
}

fun sourceUrl(): String {
  return "git config --get remote.origin.url".runShell() ?: "unknown"
}

fun revision(): String {
  return "git rev-parse --short HEAD".runShell() ?: "unknown"
}

fun whoAmI(): String {
  return "whoami".runShell() ?: "unknown"
}

fun formatAuthors(authors: Collection<Author>, pretty: Boolean = false): String {
  // Your Name <email@example.com> (http://example.com)
  val multipleAuthors: Boolean = authors.size > 1
  val prefix: String = if (pretty && multipleAuthors) "\n" else ""
  val separator: CharSequence = if(pretty) "\n" else  ", "
  val formattedAuthors: String = authors.joinToString(separator = separator) { (name, email, website) ->
    val formatted = "$name <${email}> (${website})"
    formatted.prependIndent(if(pretty && multipleAuthors) "\t- " else "")
  }
  return prefix+formattedAuthors
}

fun printMap(map: Map<String, String>) {
  var n = 0
  for((k,_) in map) {
    n = if (k.length > n) k.length else n
  }
  for ((k, v) in map) {
    println(String.format("> %-" + n + "s = %s", k, v))
  }
}

// use publishing info to derive standardized container labels

// https://www.opencontainers.org/
// https://github.com/opencontainers/image-spec/blob/master/annotations.md#annotations
fun ociAnnotations(publish: Publish): MutableMap<String, String> {
  val ociAnnotations: MutableMap<String, String> = mutableMapOf()
  ociAnnotations["org.opencontainers.image.created"] = publish.created
  ociAnnotations["org.opencontainers.image.title"] = publish.title
  ociAnnotations["org.opencontainers.image.description"] = publish.description
  ociAnnotations["org.opencontainers.image.url"] = publish.url
  ociAnnotations["org.opencontainers.image.vendor"] = publish.vendor
  ociAnnotations["org.opencontainers.image.version"] = publish.version
  ociAnnotations["org.opencontainers.image.licenses"] = publish.licenses
  // only apply them in our CI/CD environment because they aren't meaningful in local builds
  if(isCI()) {
    ociAnnotations["org.opencontainers.image.documentation"] = publish.documentation
    ociAnnotations["org.opencontainers.image.authors"] = publish.authors
    ociAnnotations["org.opencontainers.image.source"] = publish.source
    ociAnnotations["org.opencontainers.image.revision"] = publish.revision
  }
  return ociAnnotations
}

// use publishing info to derive jib's `to.image` destination as ~ `registry/vendor/name:tag`
fun repository(publish: Publish): String {
  return if(publish.registryUrl == Registries.GITLAB) {
    // GitLab Container Registry has additional pathway to project name
    "${publish.registryUrl}/${publish.vendor}/forgo/${publish.title}:${publish.version}"
  } else {
    // Docker Hub doesn't distinguish groups of containers by project
    "${publish.registryUrl}/${publish.vendor}/${publish.title}:${publish.version}"
  }
}

// use publishing info to derive simple image name without registry info `vendor/name:tag`
fun image(publish: Publish): String {
  return if(publish.registryUrl == Registries.GITLAB) {
    // GitLab Container Registry has additional pathway to project name
    "${publish.vendor}/${publish.project}/${publish.title}:${publish.version}"
  } else {
    // Docker Hub doesn't distinguish groups of containers by project
    "${publish.vendor}/${publish.title}:${publish.version}"
  }
}