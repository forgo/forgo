plugins {
  kotlin("jvm") version "1.3.61"
}

repositories {
  jcenter()
}

dependencies {
  implementation("com.google.code.gson:gson:2.8.6")
}