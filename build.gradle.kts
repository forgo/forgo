import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import com.moowork.gradle.node.npm.NpmTask
import com.moowork.gradle.node.task.NodeTask
import java.time.Instant
import java.time.format.DateTimeFormatter

plugins {
    kotlin("jvm") version "1.3.61"

    // jib plugins
    id("com.google.cloud.tools.jib") version("1.8.0") apply(false)

    // node plugins
    id("com.moowork.node") version("1.3.1") apply(false)

    // micronaut plugins
    id("com.github.johnrengelman.shadow") version("5.2.0") apply(false)
}

// resolve build dependencies from Bintray jcenter
repositories {
    jcenter()
}

group = "io.forgo"
version = Versions.DEFAULT

val authors: List<Author> = listOf(
    Author(
        name = "Elliott Richerson",
        email = "elliott.richerson@gmail.com",
        website = "https://github.com/forgo"
    )
)

description = """

------------------------------------------------------------
Experimental project to dive into the latest practices in 
software engineering.
------------------------------------------------------------
group:   $group
version: $version
authors: ${formatAuthors(authors, pretty = true)}

Gradle version: ${gradle.gradleVersion}
------------------------------------------------------------

""".trimIndent()



extra.apply {

}

val projectDescriptions: Map<String, String> = mapOf(
    Pair("client", "A browser UI for the forgo project."),
    Pair("user", "An API to manage users of the forgo project.")
)

// only apply plugins, configuration, tasks, etc. to projects that need it
val javaProjects: List<String> = listOf("client", "user")
val jibProjects: List<String> = listOf("client", "user")
val nodeProjects: List<String> = listOf("client")
val micronautProjects: List<String> = listOf("user")

subprojects {

    // resolve all subproject dependencies from Bintray jcenter
    repositories {
        jcenter()
    }

    // capture build metadata for common naming conventions, publishing, etc.
    val created: String = DateTimeFormatter.ISO_INSTANT.format(Instant.now())
    val projectName: String = "${rootProject.name}-${name}"
    val projectVersion: String = rootProject.version as String
    val description: String = projectDescriptions.getOrDefault(name, defaultValue = "The ${rootProject.name}-${name} project.")

    // version control
    val sourceUrl: String = sourceUrl() // git remote repository url
    val revision: String = revision()     // git commit hash

    val gitUrl: String = "https://gitlab.com/forgo/forgo.git"
    val projectUrl: String = "https://gitlab.com/forgo/forgo"
    val issuesUrl: String = "https://gitlab.com/forgo/forgo/issues"

    val url: String = "http://forgo.io/"
    val docs: String = "http://docs.forgo.io/v${projectVersion}"

    if (javaProjects.contains(name)) {
        // apply java gradle plugin to projects using java
        apply(plugin = "java")
    }
    if (jibProjects.contains(name)) {
        // apply jib gradle plugin to projects using jib
        apply(plugin = "com.google.cloud.tools.jib")

        // apply a common publishing pattern to all projects using jib
        val publish = Publish(
            created = created,
            title = projectName,
            project = rootProject.name,
            description = description,
            documentation = docs,
            authors = formatAuthors(authors),
            url = url,
            source = sourceUrl,
            revision = revision,
            vendor = rootProject.name,
            version = projectVersion,
            licenses = License.MIT,
            registryUrl = Registries.GITLAB,
            username = environment("DOCKER_USER"),
            password = environment("DOCKER_PASSWORD")
        )
        extra.apply {
            set("publish", publish)
        }
    }
    if (nodeProjects.contains(name)) {
        // apply node gradle plugin to projects using node/npm
        apply(plugin = "com.moowork.node")

        // apply a common node/npm version to all projects using node
        configure<com.moowork.gradle.node.NodeExtension> {
            version = Versions.NODE
            npmVersion = Versions.NPM
            workDir = file("${rootProject.buildDir}/nodejs")
            npmWorkDir = file("${rootProject.buildDir}/npm")
            nodeModulesDir = file("${project.projectDir}")
        }

        val packageJSON = PackageJSON(
            name = projectName,
            version = projectVersion,
            description = description,
            author = formatAuthors(listOf(authors.first())),
            license = License.MIT,
            homepage = projectUrl,
            repositoryUrl = gitUrl,
            repositoryDirectory = name,
            bugsUrl = issuesUrl
        )

        tasks.register<PackageJSONTask>("updatePackageJSON") {
            updates = packageJSON
        }

        tasks.withType<NpmTask> {
            dependsOn("updatePackageJSON")
        }

        tasks.withType<NodeTask> {
            dependsOn("updatePackageJSON")
        }

        tasks.getByName("check") {
            dependsOn("npm_run_check")
        }

        tasks.getByName("assemble") {
            dependsOn("npm_run_assemble")
        }

        task<Tar>("tar") {
            dependsOn("assemble")
            from(file(projectDir))
            setExcludes(listOf(".next", "__mocks__", "__tests__", "build", "node_modules"))
            compression = Compression.GZIP
            archiveBaseName.set(projectName)
            archiveExtension.set("tar.gz")
            archiveVersion.set("")
            archiveFileName.set("${archiveBaseName.get()}.${archiveExtension.get()}")
            destinationDirectory.set(file("${buildDir}/libs"))
        }

        // sync files to the client container jib staging directory
        val jibExtraDir: String = "${buildDir.path}/jib-extra-dir"
        task<Sync>("sync") {
            into(jibExtraDir)

            // untar and sync the client src
            into("/srv/www/${projectName}") {
                dependsOn("tar") // can't untar without tar
                from(tarTree(file("${buildDir}/libs/${projectName}.tar.gz")))
            }

            // copy the nginx config
            into("/srv/www/${projectName}") {
                from(file("next.sh"))
            }
        }

        // jib & jibDockerBuild rely on synced files in staging directory
        tasks
            .matching { task -> task.name.startsWith("jib") }
            .configureEach { dependsOn("sync") }

        extra.apply {
            set("packageJSON", packageJSON)
            set("jibExtraDir", jibExtraDir)
        }

    }
    if(micronautProjects.contains(name)) {

        apply(plugin = "application")

        // for creating fat/uber JARs
        apply(plugin = "com.github.johnrengelman.shadow")

        val developmentOnly: Configuration by configurations.creating

        dependencies {
            annotationProcessor(platform("io.micronaut:micronaut-bom:${Versions.MICRONAUT}"))
            annotationProcessor("io.micronaut:micronaut-inject-java")
            annotationProcessor("io.micronaut:micronaut-validation")
            implementation(platform("io.micronaut:micronaut-bom:${Versions.MICRONAUT}"))
            implementation("io.micronaut:micronaut-management")
            implementation("io.micronaut.kubernetes:micronaut-kubernetes-discovery-client")
            implementation("io.micronaut:micronaut-inject")
            implementation("io.micronaut:micronaut-validation")
            implementation("io.micronaut:micronaut-runtime")
            implementation("javax.annotation:javax.annotation-api")
            implementation("io.micronaut:micronaut-http-server-netty")
            implementation("io.micronaut:micronaut-http-client")
            runtimeOnly("ch.qos.logback:logback-classic:1.2.3")
            testAnnotationProcessor(platform("io.micronaut:micronaut-bom:${Versions.MICRONAUT}"))
            testAnnotationProcessor("io.micronaut:micronaut-inject-java")
            testImplementation(platform("io.micronaut:micronaut-bom:${Versions.MICRONAUT}"))
            testImplementation("org.junit.jupiter:junit-jupiter-api")
            testImplementation("io.micronaut.test:micronaut-test-junit5")
            testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
        }

        // use JUnit 5 platform
        tasks.withType<Test> {
            classpath += developmentOnly
            useJUnitPlatform()
        }
        tasks.withType<JavaCompile> {
            options.encoding = "UTF-8"
            options.compilerArgs.add("-parameters")
        }

        tasks.withType<ShadowJar> {
            mergeServiceFiles()
        }

        tasks.named<JavaExec>("run") {
            classpath += developmentOnly
            jvmArgs("-noverify", "-XX:TieredStopAtLevel=1", "-Dcom.sun.management.jmxremote")
        }
    }
}